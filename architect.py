"""The Architect game: combinatorial game theory and balance simulations.

The Architect game is a 1v1 competitive game, where the 2 players each hold up
to 4 numbers cards and have a common floor plan of rooms with known areas.  The
goal for a player is to spend 2 or more cards at a time to obtain an exact area
solution for a room, until all of the rooms are built.  At first, players must
multiply the numbers on any 2 of their cards together to get the area of a
room.  As the game progresses they have additional modifiers to add or subtract
one card from another before multiplying with a third card.  A round ends when
all the rooms of the floor plan are built, and the player with the highest
number of rooms wins; the number of rooms must be odd so that ties are not
possible; if there are 7 rooms, the first player to build 4 rooms wins.

The design goals are to:

1) Ensure the game always winnable: by shuffling the deck millions of times.
2) Balance fairness: player1 and player2 should win as equally as possible.
3) Maximize fun by:
   a) Minimizing the number of times each player has to draw (which makes the
      game run longer).
   b) Optimizing the number of calculations possible to play a turn to around 6
      (so that there is some calculation, but it's not tedious for the player).
   c) Having combinatorial entropy from the modifier cards and otherwise low
      cards (so that the probability of winning changes dramatically from a
      card draw) and a strategy of holding onto certain cards pays out.

Modifiers that are unlocked are addition and subtraction.  Upon building their
first room, the player unlocks addition; the first time addition is unlocked in
the game, the other player draws an extra card.  Then upon building their
second room, the player gets subtraction; the first time subtraction is
unlocked in the game, the other player draws an extra card.

"""

from collections import Counter
import itertools as it
from pprint import pformat
from warnings import warn

import numpy as np


DTYPE = np.uint8


def create_deck(seed=123, shuffle=True, min_card=1, max_card=10, n_repeats=4):
    """Generate a deck of cards."""
    deck = np.repeat(
        np.arange(min_card, max_card + 1, dtype=DTYPE),
        n_repeats)
    if shuffle:
        np.random.seed(seed)
        np.random.shuffle(deck)
    return deck


class Player():                 # pylint: disable=too-few-public-methods
    """Track resources of a single AI player."""
    MIN_HAND = 2
    MAX_HAND = 4

    def __init__(self, discard_hook):
        self.hand = np.array([], dtype=DTYPE)
        assert callable(discard_hook)
        self.discard = discard_hook

    def draw(self, cards):
        """Draw card(s) from the deck and discard if necessary."""
        if self.hand.size > Player.MAX_HAND:
            raise BufferError(('Player already has more cards than '
                               'max hand size!  Cannot draw!'))
        size_overfull_maybe = self.hand.size + cards.size
        num_discards = cards.size
        must_discard = False
        if size_overfull_maybe > Player.MAX_HAND:
            must_discard = True
            num_discards = size_overfull_maybe - Player.MAX_HAND
        self.hand = np.append(self.hand, cards)
        if must_discard:
            self.hand, discarded = self.discard(self.hand, num_discards)
            return discarded
        return None


def discard_from_beginning(hand, num):
    """Discard the first card in the hand.  Return hand and discards."""
    return hand[num:], hand[:num]


class Game():           # pylint: disable=too-many-instance-attributes
    """Simulate a single game of Architect."""
    MAX_TURNS = 100

    def __init__(self, seed=123, n_rooms=7):
        self.deck = create_deck(seed=seed)
        self.deck_orig = self.deck.copy()
        self.discards = np.array([], dtype=DTYPE)
        self.rooms = np.array([16, 18, 21, 30, 35, 40, 63],
                              dtype=DTYPE)
        assert self.rooms.size == n_rooms
        assert is_achievable_areas(np.unique(self.rooms),
                                   products(self.deck))
        self.room_owner = np.zeros_like(self.rooms)
        self.players = [
            Player(discard_from_beginning),
            Player(discard_from_beginning),
        ]
        assert len(self.players) == 2
        self.current_player = 0
        self.turn = 0
        self.deal()

    def __repr__(self):
        return 'Architect(%s)' % (pformat(self.__dict__))

    def __str__(self):
        return f"""
== Architect Game ==
State: {'complete' if self.is_over else 'running'}
Turn: {self.turn}
Score: {'-'.join([str(score) for score in self.score])}
Room areas and player owners:
{np.vstack((self.rooms, self.room_owner))}
Deck [{self.deck.size:#2d}]: {self.deck}
Discards [{self.discards.size:#2d}]: {self.discards}
Player 1 [{self.players[0].hand.size}]: {self.players[0].hand}
Player 2 [{self.players[1].hand.size}]: {self.players[1].hand}
""".strip()

    @property
    def player(self):
        """Get current player."""
        return self.players[self.current_player]

    def deal(self):
        """Deal cards to both players."""
        self.current_player = 0
        self.draw(2)
        self.current_player = 1
        self.draw(3)
        self.current_player = 0

    def draw(self, num=1):
        """Draw card(s) from the deck for the current player."""
        while num:
            num -= 1
            if self.deck.size == 0:
                self.deck, self.discards = self.discards, self.deck
            card = self.deck[0]
            discarded = self.player.draw(card)
            self.deck = self.deck[1:]
            if discarded:
                self.discards = np.append(self.discards, discarded)

    def build_room(self, room):
        """Build a room for the current player."""
        if self.room_owner[room]:
            raise ValueError('This room was already built!')
        if self.is_over:
            raise ValueError('Game is already over!')
        self.room_owner[room] = self.current_player + 1

    @property
    def rooms_majority(self):
        """Number of rooms required to win."""
        return int(np.ceil(self.rooms.size / 2))

    @property
    def score(self):
        """Summarize score of both players."""
        counts = Counter(self.room_owner)
        return [counts[player] for player in range(1, 3)]

    @property
    def is_over(self):
        """Check whether the game is over by rooms built."""
        return np.any(np.array(self.score) >= self.rooms_majority)

    @property
    def rooms_remaining(self):
        """Areas of unclaimed rooms."""
        return self.rooms[self.room_owner == 0]

    def run(self):
        """Run until game completes or the maximum allowed turns."""
        while (self.turn < Game.MAX_TURNS and
               not self.is_over):
            self.execute_turn()
        if self.turn == Game.MAX_TURNS:
            warn('Ran out of turns before completing game!', RuntimeWarning)

    @property
    def rooms_possible(self):
        """Indices of rooms that can be built by the current player."""
        return np.intersect1d(products(self.player.hand),
                              self.rooms_remaining,
                              return_indices=True)[2]

    def execute_turn(self):
        """Run a single turn."""
        rooms_possible = self.rooms_possible
        if rooms_possible.size:
            # For now, naively choose the first available room.
            area = self.rooms_remaining[self.rooms_possible[0]]
            room = np.where(self.rooms == area)[0][0]
            self.build_room(room)
        else:
            self.draw()
        self.current_player = (self.current_player + 1) % 2
        self.turn += 1


def pairs(cards, unique=True):
    """Choose all combinations of 2 cards."""
    cards = np.array(list(it.combinations(cards, 2)), dtype=DTYPE)
    if unique:
        cards = np.unique(cards, axis=0)
    return cards


def products(cards, unique=True):
    """Row-wise products of all pairs."""
    prod = np.product(pairs(cards, unique=unique), axis=1)
    if unique:
        prod = np.unique(prod)
    return prod


def is_achievable_areas(areas, products_, assume_unique=True):
    """Verify all areas are contained within possible products."""
    not_achievable = np.setdiff1d(areas, products_,
                                  assume_unique=assume_unique)
    return not_achievable.size == 0


def prob_achievable_areas(areas, cards):
    """Probability calculation ignores discarding cards for now."""
    products_ = products(cards, unique=False)
    counts = Counter(products_)
    counts_areas = np.array([counts[area] for area in areas])
    return counts_areas / products_.size
