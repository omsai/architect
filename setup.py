"""The Architect game: combinatorial game theory and balance simulations."""

import sys
from setuptools import find_packages, setup


# Boilerplate for pytest-runner suggested by pypi page.
NEEDS_PYTEST = {'pytest', 'test', 'ptr'}.intersection(sys.argv)
# Also add pytest itself because it is a runtime dependency of pytest-runner.
PYTEST_RUNNER = ['pytest-runner', 'pytest'] if NEEDS_PYTEST else []

setup(
    name="architect",
    version="0.1.dev0",
    description=__doc__,
    packages=find_packages(),
    setup_requires=PYTEST_RUNNER,
    install_requires=[
        'numpy',
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-flake8',
        'pytest-pylint',
    ],
)
