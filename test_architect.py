"""Test the architect game."""

import numpy as np
import pytest

from architect import (
    DTYPE, Game, Player, discard_from_beginning, is_achievable_areas, pairs,
    prob_achievable_areas, products)
# pylint: disable=missing-function-docstring


def test_player_initializes():
    player = Player(lambda: None)
    assert player


def test_player_draw_exceeding_hand_limit_raises_error():
    player = Player(lambda: None)
    player.hand = np.arange(1, Player.MAX_HAND + 2)
    with pytest.raises(BufferError, match='hand'):
        player.draw(np.array([Player.MAX_HAND + 2]))


def test_player_draw_discards_from_hand():
    player = Player(discard_from_beginning)
    player.hand = np.arange(1, Player.MAX_HAND + 1)
    assert player.hand.size == Player.MAX_HAND
    player.draw(np.array([Player.MAX_HAND + 1]))
    assert player.hand.size == Player.MAX_HAND


def test_discard_from_beginning():
    hand = np.arange(1, 11)
    expected = np.arange(2, 11), np.arange(1, 2)
    result = discard_from_beginning(hand, 1)
    for i in range(2):
        assert np.all(expected[i] == result[i])
    expected = np.arange(4, 11), np.arange(1, 4)
    result = discard_from_beginning(hand, 3)
    for i in range(2):
        assert np.all(expected[i] == result[i])


def test_game_initializes():
    game = Game()
    assert game


def test_game_draw_from_empty_deck_uses_discards():
    game = Game()
    game.deck = np.array([])
    game.discards = np.array([1, 5])
    game.draw()
    assert not game.discards.size
    assert game.deck.size


def test_game_draw_fetches_multiple_cards():
    game = Game()
    assert game.player.hand.size
    game.player.hand = np.array([], dtype=DTYPE)
    assert not game.player.hand.size
    game.draw(3)
    assert game.player.hand.size == 3


def test_game_build_owned_room_raises_error():
    game = Game()
    assert np.all(game.room_owner == 0)
    game.build_room(0)
    game.build_room(3)
    game.build_room(5)
    with pytest.raises(ValueError, match='room'):
        game.build_room(0)


def test_game_build_room_after_game_over_raises_error():
    game = Game()
    assert np.all(game.room_owner == 0)
    for room in range(game.rooms_majority):
        game.build_room(room)
    with pytest.raises(ValueError, match='over'):
        game.build_room(room + 1)


def test_game_run_warns_when_out_of_turns():
    game = Game()
    game.turn = Game.MAX_TURNS
    with pytest.warns(RuntimeWarning, match='turns'):
        game.run()


def test_game_run_completes_within_turn_limit():
    game = Game()
    game.run()
    assert game.turn < Game.MAX_TURNS


def test_game_execute_turn_alternates_players():
    game = Game()
    previous_player = game.current_player
    game.execute_turn()
    assert game.current_player != previous_player


def test_game_execute_turn_builds_room_when_possible():
    game = Game()
    game.rooms = np.array([12, 20, 21, 25, 49, 50, 70])
    game.room_owner = np.array([1, 2, 2, 0, 1, 1, 2])
    game.players[0].hand = np.array([1, 1])
    game.players[1].hand = np.array([5, 5])
    game.execute_turn()
    expected = np.array([1, 2, 2, 0, 1, 1, 2])
    assert np.all(expected == game.room_owner)
    game.execute_turn()
    expected = np.array([1, 2, 2, 2, 1, 1, 2])
    assert np.all(expected == game.room_owner)


def test_game_score():
    game = Game()
    game.room_owner = np.array([1, 2, 2, 0, 1, 1, 2])
    assert game.score == [3, 3]
    game.room_owner = np.array([1, 1, 2, 0, 0, 0, 0])
    assert game.score == [2, 1]
    game.room_owner = np.array([1, 2, 2, 2, 0, 0, 0])
    assert game.score == [1, 3]


def test_game_repr():
    game = Game()
    assert game.__repr__()


def test_game_str():
    game = Game()
    assert game.__str__()


def test_pairs():
    cards = np.array([2, 2, 3, 3])
    expected = np.array(
        [[2, 2],
         [2, 3],
         [3, 3]])
    result = pairs(cards)
    assert np.all(expected == result)


def test_products():
    cards = np.array([2, 2, 3, 3])
    expected = np.array([4, 6, 9])
    result = products(cards)
    assert np.all(expected == result)


def test_is_achievable_areas():
    areas = np.array([4, 6, 9])
    cards = np.array([2, 2, 3, 3])
    assert is_achievable_areas(areas, products(cards))
    areas = np.array([4, 6, 9])
    cards = np.array([2, 3, 3])
    assert not is_achievable_areas(areas, products(cards))
    cards = np.array([2, 3])
    assert not is_achievable_areas(areas, products(cards))
    areas = np.array([6])
    assert is_achievable_areas(areas, products(cards))


def test_prob_achievable_areas():
    areas = np.array([4, 6, 9])
    cards = np.array([2, 2, 3, 3])
    expected = np.array([1/6, 2/3, 1/6])
    result = prob_achievable_areas(areas, cards)
    assert np.all(expected == result)
    cards = np.array([2, 3, 3])
    expected = np.array([0, 2/3, 1/3])
    result = prob_achievable_areas(areas, cards)
    assert np.all(expected == result)
